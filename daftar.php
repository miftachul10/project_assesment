<?php

include "crud/connection.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>Detak.IB</title>
</head>
<body>
  <div class="container-fluid ">
    <div class="row">
      <div class="col">
        <?php require "header.php";?>
              <!-- form input daftar -->
              <div id="daftar" class="container">
                <div class="row vh-100 justify-content-center ">
                  <div id="daftar2" class="col-5 border rounded p-4 align-self-center">
                    <h1>Daftar sekarang</h1>
                    <form action="crud/input.php" method="POST">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" autocomplete="off" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="text" autocomplete="off" name="email" class="form-control" id="exampleInputPassword1" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Mapel</label>
                            <select name="mapel" class="form-control">
                              <option  value="Matematika">Matematika</option>
                                <option  value="Fisika">Fisika</option>
                                <option  value="Biologi">Biologi</option>
                                <option  value="Bahasa Indonesia">Bahasa Indonesia</option>
                                <option  value="Bahasa Inggris">Bahasa Inggris</option>
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Paket</label>
                              <select name="paket" class="form-control">
                                <option  value="Paket A">Paket A</option>
                                <option  value="Paket B">Paket B</option>
                                <option  value="Paket C">Paket C</option>
                              </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                      </form>
                  </div>
                </div>
              </div>            
      </div>
    </div>
  </div>
  
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>