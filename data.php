<?php 

include "crud/connection.php";
include "crud/search.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <style>

    </style>
    <title>Detak.IB</title>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col">
        <?php require "header.php";?>
        <!-- Content -->
        <div id="data" class="container " >
          <div class="row">
            <div class="col">
              <h1 class="text-center mt-3" style="font-family: calibry;"><b>Data peserta</b></h1>
              <form class="form-inline my-2 my-lg-0 mt-3 " action="data.php" method="POST">
                <input class="form-control mr-sm-2" autocomplete="off" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-secondary my-2 my-sm-0" name="cari" type="submit">cari</button>
              </form>
              <!-- Allert Message -->
              <?php if(isset($row)):?>
              <div class="alert alert-primary alert-dismissible fade show mt-3" role="alert">
                <p class="lead"><?php echo $row; ?>Data Ditemukan</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php endif;?>
              <table class="table table-striped bg-light mt-3  border border-secondary">
                <thead class="bg-secondary">
                  <tr  class="text-light">
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mapel</th>
                    <th scope="col">Paket</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data_daftar as $key) : ?>
                    <tr>
                      <td><?php echo $key["nama"]; ?></td>
                      <td><?php echo $key["email"]; ?></td>
                      <td><?php echo $key["mapel"]; ?></td>
                      <td><?php echo $key["paket"]; ?></td>                    
                    </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>                            
            </div>
          </div>
        </div>
  
      </div>
    </div>
  </div> 
 
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>