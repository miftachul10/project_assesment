<?php 
include "crud/connection.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>Detak.IB</title>
</head>
<body class="bg-light">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <!-- header -->
        <div class="container-fluid bg-secondary">
            <div class="row">
              <div class="col">
                <nav class="navbar fixed-top navbar-expand navbar-info" id="navbar">
                  <a class="navbar-brand " href="#"><img src="img/logo1.png" alt="image" width="50" height="50" ></a>

                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item active mr-2">
                        <a class="nav-link"><h2><i>Detak IB</i></h2></a>
                      </li>
                      <li class="nav-item active mt-1">
                        <a class="nav-link" href="index.php"><h3>HOME</h3> <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item active mt-3">
                        <a class="nav-link" href="daftar.php">DAFTAR</a>
                      </li>
                    </ul>
                  </div>
                </nav>       
              </div>
            </div>
          </div>
        <!-- Content -->
        <div class="jumbotron jumbotron-fluid">
          <div class="container" style="color: white;">
            <br><br><br><br><br>
            <h1 class="display-4" style="font-weight:bolder; font-family: fixedsys, monospace;">WELCOME to Kelas Detak.IB</h1>
            <p class="" style="font-weight:100px;">Kelas Online dan Offline dengan Metode Pembelajaran Konsep yang Sederhana & Guru yang Berpengalaman</p>
            <a href="daftar.php" class="btn btn-primary">Daftar Sekarang</a>
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
          </div>
        </div>    
    
        <div id="paket" class="container">
          <div class="row">
            <div class="col-4 ">
              <div class="card mx-auto text-center" style="width: 18rem;">
                <i class="fas fa-user mt-2" style="font-size: 3em; color: green;"></i>
                <div class="card-body">
                  <h5 class="card-title">PAKET A</h5>
                  <p class="card-text">Layanan belajar Untuk Seluruh Indonesia ataupun siswa yang studi di luar negeri.</p>
                  <a href="daftar.php" class="btn btn-primary rounded-pill">Daftar</a>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="card mx-auto text-center" style="width: 18rem;">
                <i class="fas fa-user-friends mt-2" style="font-size: 3em; color: purple;"></i>
                <div class="card-body">
                  <h5 class="card-title">PAKET B</h5>
                  <p class="card-text">Layanan belajar 1 to 1 Khusus JaBoDeTaBek, Kota Wisata.Daerah lain menyesuaikan .</p>
                  <a href="daftar.php" class="btn btn-primary rounded-pill">Daftar</a>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="card mx-auto text-center" style="width: 18rem;">
                <i class="fas fa-users mt-2" style="font-size: 3em; color: red;"></i>
                <div class="card-body">
                  <h5 class="card-title ">PAKET C</h5>
                  <p class="card-text">Layanan Privat untuk lebih dari 1 siswa (Maksimal 4 Siswa).</p><br>
                  <a href="daftar.php" class="btn btn-primary text-align rounded-pill">Daftar</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="container">
          <div class="row text-center">
            <div id="tentang" class="col-12">
              <h2>Tentang Kami</h2>
              <p>Kami Menyediakan Jasa Les Privat bagi anak sekolah ataupun mahasiswa yang mengalami kesulitan dalam menghadapi pelajarannya.</p>
            </div>
            <div id="misi" class="col-6 border rounded text-light ">
              <h3>Misi Kami</h3>
              <p>Mampu mengubah mindset siswa tentang konsep pelajaran matematika dan menjadi guru privat terbaik untuk mendapatkan nilai terbaik dari potensi siswa.</p>
            </div>
            <div id="visi" class="col-6 border rounded text-light">
              <h3>Visi Kami</h3>
              <p>Melayani sebanyak mungkin siswa yang mengalami kesulitan pelajaran di sekolah ataupun di kampus baik untuk pelajaran matematika maupun pelajaran lainnya.</p>
            </div>
            <div id="tentang" class="col-12">
              <h2>Mengapa memilih Kelas detak.id ?</h2>
            </div>
            <div id="keungulan" class="col-6 border rounded text-light">
              <h3>Keunggulan</h3>
              <p>1. Mengikuti adaptasi kurikulum baru sesuai sekolah siswa serta dibimbing oleh guru yang sudah berpengalaman mengajar bertahun tahun sesuai kebutuhan siswa.</p>
              <p>2. Metode belajar yang di terapkan adalah dimulai dengan pemahaman definisi konsep yang sederhana serta penerapan aplikasi dalam realita khususnya pelajaran matematika.</p>
              <p>3. Dengan Detak IB siswa dapat lebih fokus belajar dan menanyakan masalah belajar karena konsentrasi guru sepenuhnya 100% kepada murid saat les privat sehingga bisa bertanya dan berdiskusi dengan sepuasnya.</p>
            </div>
            <div id="kelebihan" class="col-6 border rounded text-light">
              <h3>Kelebihan</h3>
              <p>1. Guru sudah fix tersedia dan siap untuk melakukan les privat saat di hubungin ataupun di booking sesuai jam kosong yang tersedia. Tidak perlu menunggu berhari hari untuk mencari tutor yang di perlukan atau kepastian adanya tutor yang tersedia untuk mengajar.</p>
              <p>2. Siswa diberikan akses materi e-book ataupun latihan soal berupa soal dari berbagai sekolah hingga soal tahun lalu yang tentunya berguna untuk kebutuhan siswa saat belajar ataupun untuk persiapan ujian.</p>
              <p>3. Siswa diberikan kebebasan untuk berdiskusi dengan guru di luar jam les privat. Bisa diskusi soal melalui chat WA yang sudah disediakan.</p>
            </div>
          </div>
        </div>
        
        <div class="container">
            <h3 class="mt-5">Download Aplikasi</h3>
            <div class="row">
                <div class="col-12">
                    <a href="https://play.google.com/store" target="_blank"><img src="img/playstore.png" alt="google" height="40" ></a>
                    <a href="https://www.apple.com/app-store/"><img src="img/appstore.png" alt="google" height="40"></a>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 col-lg-10">
                    <p>copyright &copy; 2020 Aplikasi CRUD: Miftachul Rohman</p>
                </div>
                <div class="col-12 col-md-2">
                    <i class="fab fa-cc-visa"></i>
                    <i class="fab fa-cc-paypal"></i>
                    <i class="fab fa-cc-mastercard"></i>
                </div>
            </div>
        </div>       
      </div>
    </div>
  </div>
        
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>