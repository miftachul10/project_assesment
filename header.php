<?php 

include "crud/connection.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>Detak.IB</title>
</head>
<body>
  <div class="container-fluid bg-secondary">
    <div class="row">
      <div class="col">
        <nav class="navbar fixed-top navbar-expand navbar-info" id="navbar">
          <a class="navbar-brand " href="#"><img src="img/logo1.png" alt="image" width="50" height="50" ></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active mr-2">
                <a class="nav-link"><h2><i>Detak IB</i></h2></a>
              </li>
              <li class="nav-item active mt-1 ml-2">
                <a class="nav-link" href="index.php"><h3>Exit</h3> <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active mt-3">
                <a class="nav-link" href="daftar.php">DAFTAR</a>
              </li>
              <li class="nav-item active mt-3">
                <a class="nav-link" href="data.php"aria-disabled="true">DATA PESERTA</a>
              </li>
              <li class="nav-item active mt-3">
                <a class="nav-link" href="setting.php"aria-disabled="true">SETTING</a>
              </li>
            </ul>
          </div>
        </nav>       
      </div>
    </div>
  </div>
  
  <!-- Option 1: jQuery, Popper.js, and Bootstrap JS-->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>